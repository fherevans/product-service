package webservices.productservice.dto;

public class UpdateRequest {

    private int cantidad;

    public UpdateRequest( ) {}

    public UpdateRequest(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
