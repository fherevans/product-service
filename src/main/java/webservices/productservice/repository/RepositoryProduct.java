package webservices.productservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import webservices.productservice.dto.DtoProduct;

import java.util.List;

@Repository
public class RepositoryProduct {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<DtoProduct> getProducts() throws Exception {
        try {
            return jdbcTemplate.query("SELECT * FROM product;", new BeanPropertyRowMapper<DtoProduct>(DtoProduct.class));
        } catch (Exception e) {
            throw new Exception(e.getLocalizedMessage());
        }
    }

    public DtoProduct getProduct(String code) throws Exception {
        try{
            return jdbcTemplate.queryForObject("SELECT * FROM product WHERE codigo=" + code + ";", new BeanPropertyRowMapper<DtoProduct>(DtoProduct.class));
        } catch (Exception e) {
            throw new Exception(e.getLocalizedMessage());
        }
    }

    public void createProduct(DtoProduct dtoProduct) throws Exception{
        try {
            jdbcTemplate.update("{CALL st_create_product(?,?,?,?,?,?)}",
                    dtoProduct.getCodigo(),
                    dtoProduct.getProducto(),
                    dtoProduct.getDescripcion(),
                    dtoProduct.getPrecio(),
                    dtoProduct.getCantidad(),
                    dtoProduct.getIdCategoria()
            );
        } catch (UncategorizedSQLException e) {
            throw new Exception(e.getSQLException().getLocalizedMessage());
        }
    }

    public void updateStockProduct(String code, int cantidad) throws Exception {

    }

    public void deleteProduct(String code) throws Exception {
        try {
            jdbcTemplate.update("DELETE FROM product WHERE codigo = " + code + ";");
        } catch (Exception e) {
            throw new Exception(e.getLocalizedMessage());
        }
    }
}
