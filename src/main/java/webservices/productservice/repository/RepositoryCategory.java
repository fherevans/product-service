package webservices.productservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import webservices.productservice.dto.DtoCategory;

import java.util.List;

@Repository
public class RepositoryCategory {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<DtoCategory> getCategories() throws Exception {
        try {
            return jdbcTemplate.query("SELECT * FROM category;", new BeanPropertyRowMapper<DtoCategory>(DtoCategory.class));
        } catch (Exception e) {
            throw new Exception(e.getLocalizedMessage());
        }
    }

    public DtoCategory getCategory(int id) throws Exception {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM category WHERE id=" + id + ";", new BeanPropertyRowMapper<DtoCategory>(DtoCategory.class));
        } catch (Exception e) {
            throw new Exception(e.getLocalizedMessage());
        }
    }

    public void createCategory(DtoCategory dtoCategory) throws Exception {
        try {
            jdbcTemplate.update("{CALL st_create_category(?)}", dtoCategory.getCategoria());
        } catch (UncategorizedSQLException e) {
            throw new Exception(e.getSQLException().getLocalizedMessage());
        }
    }

    public void updateCategory(DtoCategory dtoCategory) throws Exception {
        try {
            jdbcTemplate.update("{CALL st_update_category(?,?)}", dtoCategory.getCategoria(), dtoCategory.getId());
        } catch (UncategorizedSQLException e) {
            throw new Exception(e.getSQLException().getLocalizedMessage());
        }
    }

    public void deleteCategory(int id) throws Exception {
        try {
            jdbcTemplate.update("DELETE FROM category WHERE id = " + id + ";");
        } catch (Exception e) {
            throw new Exception(e.getLocalizedMessage());
        }
    }
}
