package webservices.productservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import webservices.productservice.dto.DtoProduct;
import webservices.productservice.exceptionHandler.ApiException;
import webservices.productservice.service.ServiceProduct;

@RestController
@RequestMapping("/product")
public class ControllerProduct {

    @Autowired
    ServiceProduct serviceProduct;

    @GetMapping
    public ResponseEntity<Object> getProducts() throws Exception {
        try {
            return new ResponseEntity<>(serviceProduct.getProducts(), HttpStatus.OK);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
        }
    }

    @GetMapping("/{code}")
    public ResponseEntity<Object> getProduct(@PathVariable("code") String code) throws Exception {
        try {
            return new ResponseEntity<>(serviceProduct.getProduct(code), HttpStatus.OK);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
        }
    }

    @PostMapping
    public ResponseEntity<Object> createProduct(@RequestBody DtoProduct product) throws Exception {
        try {
            serviceProduct.createProduct(product);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @PutMapping("/{code}")
    public ResponseEntity<Object> updateStockProduct(@PathVariable("code") String code, @RequestBody int cantidad) throws Exception {
        return null;
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<Object> deleteProduct(@PathVariable("code") String code) throws Exception {
        try {
            serviceProduct.deleteProduct(code);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
        }
    }
}
