package webservices.productservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import webservices.productservice.dto.DtoCategory;
import webservices.productservice.exceptionHandler.ApiException;
import webservices.productservice.service.ServiceCategory;

@RestController
@RequestMapping("/category")
public class ControllerCategory {

    @Autowired
    ServiceCategory serviceCategory;

    @GetMapping
    public ResponseEntity<Object> getCategories() throws Exception {
        try {
            return new ResponseEntity<>(serviceCategory.getCategories(), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getCategory(@PathVariable("id") int id) throws Exception {
        try {
            return new ResponseEntity<>(serviceCategory.getCategory(id), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
        }
    }

    @PostMapping
    public ResponseEntity<Object> createCategory(@RequestBody DtoCategory category) throws Exception {
        try{
            serviceCategory.createCategory(category);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateCategory(@RequestBody DtoCategory category, @PathVariable("id") int id) throws Exception {
        try {
            serviceCategory.getCategory(id);
        } catch (Exception e){
            throw new ApiException(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
        }
        try {
            serviceCategory.updateCategory(category);
        } catch (Exception e){
            throw new ApiException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

     @DeleteMapping("/{id}")
     public ResponseEntity<Object> deleteCategory(@PathVariable("id") int id) throws Exception {
         try {
             serviceCategory.deleteCategory(id);
         } catch (Exception e){
             throw new ApiException(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
         }
         return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
}
