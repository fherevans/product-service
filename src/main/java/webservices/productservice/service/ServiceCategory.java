package webservices.productservice.service;

import webservices.productservice.dto.DtoCategory;

import java.util.List;

public interface ServiceCategory {

    public List<DtoCategory> getCategories() throws Exception;

    public DtoCategory getCategory(int id) throws Exception;

    public void createCategory(DtoCategory category) throws Exception;

    public void updateCategory(DtoCategory category) throws Exception;

    public void deleteCategory(int id) throws Exception;
}
