package webservices.productservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webservices.productservice.dto.DtoProduct;
import webservices.productservice.repository.RepositoryProduct;

import java.util.List;

@Service
public class ServiceProductImpl implements ServiceProduct {

    @Autowired
    RepositoryProduct repositoryProduct;

    @Override
    public List<DtoProduct> getProducts() throws Exception {
        return repositoryProduct.getProducts();
    }

    @Override
    public DtoProduct getProduct(String code) throws Exception {
        return repositoryProduct.getProduct(code);
    }

    @Override
    public void createProduct(DtoProduct product) throws Exception {
        repositoryProduct.createProduct(product);
    }

    @Override
    public void updateStockProduct(String code, int cantidad) throws Exception {

    }

    @Override
    public void deleteProduct(String code) throws Exception {
        try {
            repositoryProduct.deleteProduct(code);
        } catch (Exception e) {
            throw new Exception("El producto no existe");
        }
    }
}
