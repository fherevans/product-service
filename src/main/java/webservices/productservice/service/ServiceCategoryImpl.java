package webservices.productservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webservices.productservice.dto.DtoCategory;
import webservices.productservice.repository.RepositoryCategory;

import java.util.List;

@Service
public class ServiceCategoryImpl implements ServiceCategory {
    @Autowired
    RepositoryCategory repositoryCategory;

    @Override
    public List<DtoCategory> getCategories() throws Exception {
        return repositoryCategory.getCategories();
    }

    @Override
    public DtoCategory getCategory(int id) throws Exception {
        try {
            return repositoryCategory.getCategory(id);
        } catch (Exception e) {
            throw new Exception("La categoría no existe");
        }
    }

    @Override
    public void createCategory(DtoCategory category) throws Exception {
        repositoryCategory.createCategory(category);
    }

    @Override
    public void updateCategory(DtoCategory category) throws Exception {
        repositoryCategory.updateCategory(category);
    }

    @Override
    public void deleteCategory(int id) throws Exception {
        repositoryCategory.deleteCategory(id);
    }
}
