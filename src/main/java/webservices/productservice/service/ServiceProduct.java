package webservices.productservice.service;

import webservices.productservice.dto.DtoProduct;

import java.util.List;

public interface ServiceProduct {

    public List<DtoProduct> getProducts() throws Exception;

    public DtoProduct getProduct(String code) throws Exception;

    public void createProduct(DtoProduct product) throws Exception;

    public void updateStockProduct(String code, int cantidad) throws Exception;

    public void deleteProduct(String code) throws Exception;
}
